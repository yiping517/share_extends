package share_extends;

public class SuperDemo {

	public static void main(String[] args) {
		Boo b = new Boo();
	}

}

class Aoo{
	Aoo(){
		System.out.println("父構");
	}
}

class Boo extends Aoo{
	Boo(){
		System.out.println("子類構");
//		super();
	}
}

class Coo{
	Coo(int a){
		System.out.println("C構");
	}
}
//class Doo extends Coo{	} 
//出現錯誤。因為至此還沒寫constructor。不寫會有個默認constructor。
//而在默認constructor中首先會調用super()，但父類沒有無參。
//因為Coo中寫了個有參constructor後，系統便不會默認給無參constructor
//解法1:
class Doo extends Coo{
	Doo(){
		super(5);
	}
} 