package share_extends;

public class UpcastingDemo {

	public static void main(String[] args) {
		Animal a = new Animal();
//		a.subjectB
		//1.
		
		Bird b = new Bird();
		b.subject = "";
		//2.
		
		//3.
		Animal a2 = new Bird();
//		a2.yell();
		//4.
		
	}

}

class Animal{
	String subject = "�ʪ�";
	void run() {}
	void animalRun() {
		System.out.println(subject+"�]!!!");
	}
}
class Bird extends Animal{
	String subjectB = "��";
	void run() {
		System.out.println(subject+"��~~");
	}
	void yell() {}
}
